const { mailsToSend, user, pass } = require("../../config/criteria");

module.exports = (apparts) => {

    const nodemailer = require("nodemailer");
    //Création du transporteur SMTP
    const transporter = nodemailer.createTransport({
        service: "Gmail",
        auth: {
            user,
            pass,
        }
    });
    //Creation du mail a envoyer
    let html = "<html>";
    apparts.forEach(element => {
        console.log(element);
        html += "<h1><a href=" + (element.link || element.url) + ">" + element.title + " (id: " + element.checksum + ")</a></h1>";
        if (element.images) {
            html += "<p><img src=\"" + element.images[0] + "\"/></p>";
        }
        html += "<p> Description: " + element.description + "</p>";
        html += "<p> Prix: " + element.price + "</p>";
    });
    const mail = {
        from: "< FindAHome Bot >",
        to: mailsToSend,
        subject: "De nouveaux appartements sont disponibles",
        html: html + "</html>",
    };
    //Envoi du mail
    return (new Promise((resolve, reject) => {
        transporter.sendMail(mail, (err) => {
            if (err) {
                reject({ status: "KO", err });
            } else {
                console.log("Mail send");
                resolve({ status: "OK" });
            }
            transporter.close();
        });
    }));
};
