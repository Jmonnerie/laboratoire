/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   main.cpp                                         .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: jmonneri <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/04/02 00:57:33 by jmonneri     #+#   ##    ##    #+#       */
/*   Updated: 2019/04/02 00:57:37 by jmonneri    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include <string>
#include <iostream>
#include <fstream>

std::string     reverseAll(std::string &content) {
    int         i = content.size();

    while ( i-- > 0 ) {
        std::cout << content[i];
    }
    return content;
}

int             usage( void ) {
    std::cout << "Usage: ./Reverse_me FILENAME" << std::endl
              << "FILENAME: the name of the file to reaplace in => not empty" << std::endl;
    return -1;
}

int             main( int argc, char **argv ) {
    
    if (argc < 2) {
        return usage();
    }
    std::string fileName(argv[1]);
    if (fileName.empty()) {
        return usage();
    }
    std::cout << "Start to read the file..." << std::endl;
    std::ifstream ifs(argv[1]);
    std::string content((std::istreambuf_iterator<char>(ifs)), (std::istreambuf_iterator<char>()));
    if (content.empty()) {
        std::cout << "The file doesn't exists or is empty" << std::endl;
        return -1;
    }
    reverseAll(content);
    std::string replaceFileName = fileName.append(".reversed.txt");
    std::cout << "Reveresed! Start to write in " << replaceFileName << std::endl;
    std::ofstream outfile(replaceFileName);
    outfile << content << std::endl;
    outfile.close();
    std::cout << "Reversed !" << std::endl;
    return 0;
}