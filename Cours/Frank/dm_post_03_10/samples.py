
# DECLARATION DUNE VARIABLE (attention aux TYPES)
ma_variable_int = 42                      # Variable de type integer => nombre entier
ma_variable_float = 42.424242             # Variable de type float => nombre non entier
ma_variable_str = "Je suis une string"    # Variable de type string => phrase
ma_variable_char = 'a'                    # Variable de type character => caractère
# Note: en python, il n'y a presque pas de difference entre une string et un caractere. Tu peux ecrire:
# ma_variable_char = "c"
# ma_variable_char = 'c'
# Pourquoi? Normalement une string est un tableau de caracteres. En python un caractere est un tableau d'une seule case donc une string avec un seul caractere

# !!! Attention un float ne peut pas etre mélangé/comparé/additionné avec un int ou même une string et inversement



# DECLARATION ET UTILISATION D'UNE FONCTION

# Fonction simple additionnant 2 nombres et retournant le resultat
def addition(nbr_a, nbr_b):               # Ici on definit une fonction prenant 2 paramètres: nbr_a, nbr_b
    nbr_to_return = nbr_a + nbr_b         # Ici, on fait le traitement de la fonction.
    return ( nbr_to_return )              # Si la fonction doit retourner une valeur, on la met dans return

# Exemple d'utilisation de "addition"
def main():
    nbr_a = input("Entrez un nombre A:")  # On demande a l'user de rentrer 2 nombres A et B
    nbr_b = input("Entrez un nombre B:")
    resultat = addition(nbr_a, nbr_b)     # Ici le programme va lancer la fonction addition en lui passant les 2 nombres et mettre le resultat dans la variable resultat
    print("Le resultat de l'addition de ces 2 nombres est : " + str(resultat)) # Enfin, on imprime



# LES CONDITIONS (Voir en fin de page pour les differents operateurs ou alors sur openclassrooms)

# Une condition est une operation qui retourne True (vrai) ou False (Faux)
i = 42
if (i == 42):                             # Ici, i == 42 va retourner True donc on rentre dans le bloc if(si) et pas dans le else(sinon)
    print("On est dans le if")
else:                                     # Si i est != (different) de 42 alors on rentre dans le else
    print("on est dans le else")

# On peut chainer les conditions. Ici, on rentrera dans le premier bloc dont la condition est vérifiée:
age = input("Entre ton age")
if (age < 10):                            # Si
    print("Salut gamin!")
elif (age < 18):                          # Sinon si (else if => elif)
    print("Salut petit jeune!")
elif (age < 30):
    print("Salut jeune homme")
elif (age < 50):
    print("Bonjour Monsieur")
else:                                     # Dans tous les autres cas
    print("Salut papi!")



# LES BOUCLES

# Elles servent à boucler sur une instruction. Exemple:
# Tant que (mes dents == sale):
#     brosser les dents

while (condition):
    # Do things
    pass

# Attention a ne pas faire de boucles infinies! Exemple:
while (True):
    print("Je ne m'arrêterai pas")
    i += 1                                # Ici la condition renvoie TOUJOURS True donc on reste dans la boucle
# Il faudrait plutôt ecrire:
while (True):
    print("Je ne m'arrêterai pas")
    if (i > 200):
        break                             # BREAK: Ceci force l'arret de la boucle
    else:
        i += 1


# OPERATEURS DE CONDITION

nbr_a = input("Entrez un nombre A:")
nbr_b = input("Entrez un nombre B:")

if (nbr_a == nbr_b):       # Si a est égal a b
    pass
if (nbr_a < nbr_b):        # Si a est inférieur a b
    pass
if (nbr_a > nbr_b):        # Si a est supérieur a b
    pass
if (nbr_a <= nbr_b):       # Si a est inférieur ou égal a b
    pass
if (nbr_a >= nbr_b):       # Si a est supérieur ou égal a b
    pass
if (nbr_a != nbr_b):       # Si a est différent de b
    pass

# OPERATEURS LOGIQUES

# Des fois on a besoin de faire 2 conditions d'un coup: comment faire? Exemple
# Si j'ai suffisement d'argent ET que je suis majeur alors je peux acheter de l'alcool sinon je ne peux pas
argent = input("Combien d'argent as tu?")
age = input("Quel age as tu?")
if ( argent > 5 and age >= 18 ):
    print("A la votre!")
else:
    print("Un diabolo menthe svp")

# Voici les différents opérateurs logiques:
# and                Si les 2 conditions renvoient true, le tout est true
# or                 Si l'une des 2 conditions est true, le tout est true
# in                 Teste l'appartenance a un contenant (tableau, etc) Exemple parlant:
mes_fruits = ["banane", "poire", "peche"]
fruit_demande = input("Que veux tu manger comme fruit?")
if (fruit_demande in mes_fruits):
    print("Tiens j'ai bien une " + fruit_demande)
else:
    print("Desole je n'ai pas de " + fruit_demande)
# is                 Teste si la variable est la meme ou juste une copie. On reviendra plus tard la dessus, c'est une notion complexe
# not                a rajouter devant in ou is pour tester l'inverse:
if (fruit_demande not in mes_fruits):
    print("Desole je n'ai pas de " + fruit_demande)
else:
    print("Tiens j'ai bien une " + fruit_demande)