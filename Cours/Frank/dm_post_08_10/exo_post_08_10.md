
# Série d'exercices

Voila je t'ai passé un bon bout de connaissances et je vais te faire jouer avec. On apprend en faisant.
On ne recommencera pas de cours tant que t'auras pas fait suffisement d'exercices, sauf à ta demande bien entendu.
Je vais te donner toute une série de petits exos inscrits dans le cadre du développement d'un jeu vidéo.
Si tu t'accroches tu vas t'amuser.

## Première Partie: La souffrance

Ici, tu vas faire des exercices dont tu ne comprendras peut être pas l'utilité,
tout simplement parcequ'ils t'apprendront par la pratique.

Quelques règles générales sinon c'est pas drôle:

- Github interdit : évidemment github et tous ses dérivés sont interdits.
    Tu n'as pas besoin d'internet pour ces exercices.
    Un petit "@" apres l'exo te signale si tu as besoin d'internet ou non.
- Si tu as une question, envoie un sms.
- Tu coderas a la norme PEP8 (pycodestyle dans vscode).
    Pour ce faire, ajoute l'extension python a VScode si ce n'est pas déjà fait (bouton dans la barre des tâches à gauche de ton écran)
    Ensuite fais un Ctrl+Shift+P et écris "Python: Select Linter" puis cliques sur pycodestyle.
    Normalement, tu devrais pas trop avoir de soucis. Sinon, pose-toi les bonnes questions : "Comment mon code devient lisible?
    Sinon, lis la doc de PEP8. Ou alors ce pti site: https://larlet.fr/david/biologeek/archives/20080511-bonnes-pratiques-et-astuces-python/

### Exo 1

Ecrire un petit programme qui affichera à l'écran une suite de nombres.
Attention tu dois reproduire le même comportement que l'exemple ci dessous.
Petite contrainte : ta fonction ne doit pas faire plus de 4 lignes. Et oui c'est possible.

Exemple:

'''

    >> 0
    >> 1
    >> 2
    >> 4
    >> 8
    >> 16
    >> 32
    >> 64
    >> ...  #Ici c'est une élipse. bien entendu ton programme n'affichera pas  "..." mais les bonnes suites de chiffres
    >> 4096

'''

### Exo 2

Ecrire le programme "GrosseDame.py"
Lui faire demander: "Mot de paaaaaasse?"
Et lui faire refuser l'entrée tant qu'on n'a pas répondu "Caput Draconis" ou alors "42"

### Exo 3

Plus corsé celui-la:
On va demander une entrée à l'utilisateur: soit 1, 2, 3, ou 4 ou alors EXIT.
Après chaque entrée on affichera un nombre de caractères 4 puis 2 (séparés par des espaces et sans espace à la fin de la ligne) sur un certain nombre de lignes.
Effets:
1: réduit d'une colonne
2: augmente d'une ligne
3: augmente d'une colonne
4: réduit d'une ligne
Exemple (attention comportement à reproduire. Le ./programme.py signifie la lancée du programme):

'''

    $ ./programme.py        # Le "$ " signifie que c' est l'user qui parle
    4 2 4 2
    2 4 2 4
    4 2 4 2
    2 4 2 4

    Press number or EXIT
    $ 1
    4 2 4
    2 4 2
    4 2 4
    2 4 2

    Press number or EXIT
    $ 2
    4 2 4
    2 4 2
    4 2 4
    2 4 2
    4 2 4

    Press number or EXIT
    $ 3
    4 2 4 2
    2 4 2 4
    4 2 4 2
    2 4 2 4
    4 2 4 2

    Press number or EXIT
    $ 4
    4 2 4 2
    2 4 2 4
    4 2 4 2
    2 4 2 4

    Press number or EXIT
    $ EXIT

    OK! Bye!

'''

Bon appelle moi des que tu as terminé et renvoyé ça, je t'en renvoie par la suite
