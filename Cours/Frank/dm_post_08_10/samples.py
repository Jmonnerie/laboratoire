# LES EXCEPTIONS
try:
    a = 5 / 0                                       # Ici on teste une division par 0
except:
    print("ZeroDivisionError: division by zero")    # On attrape l'erreur et on fait un traitement. La phrase écrite ici est la même que l'erreur de type ZeroDivisionError
print("POST ERROR")                                 # En "attrapant" l' erreur, on ne fait pas planter le programme. cette ligne est bel et bien exécutée

# De cette manière, toutes les exceptions sont traitées de la même manière. Par exemple:
# Dans le cas ci-dessous, on essaye de "caster" (changer le type d'une variable) une valeur en int qui ne sera peut être pas un int.
# Si l'utilisateur entre une phrase, le cast en int ne va pas fonctionner et cela va 
try:
    number = int(input("entrez un nombre"))         
    a = 5 / 0
except:
    print("ZeroDivisionError: division by zero")    # On attrape l'erreur et on fait un traitement. La phrase écrite ici est la même que l'erreur de type ZeroDivisionError
print("POST ERROR")

print("0")
i = 1
while (i < 4096):
    i *= 2