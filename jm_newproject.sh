#!bin/sh
function ft_gitignore
{
	line=BEGIN
	while [ $line != "END" ];do
		printf "\nEnter a line to add to .gitignore (END to finish)\n\n\t"
		read line
		if [ $line != "END" ];then
			echo "$line" >> $path$name/.gitignore
		fi
	done
}

function ft_libft
{
	printf "\nWhere is your Libft?\n\n\t"
	while [ 42 ];do
		read pathlib
		if [ -e $pathlib ];then
			break 1
		fi
		printf "\nThis path doesn't exist. Please enter a valid path for your sources\n\n\t"
	done
	printf "\nCreation of $path$name/srcs/libft..."
	mkdir $path$name/srcs/Libft
	printf "\033[32m[OK]\033[0m"
	printf "\nCopying files..."
	cp -rf $pathlib $path$name/srcs/libft
	printf "\033[32m[OK]\033[0m\n"
}

echo "/******************************************************************************/"
echo "/*                                                                            */"
echo "/*          ▄▄▄██▀▀▀      ███▄ ▄███▓         ██▓    ▄▄▄       ▄▄▄▄            */"
echo "/*            ▒██        ▓██▒▀█▀ ██▒        ▓██▒   ▒████▄    ▓█████▄          */"
echo "/*            ░██        ▓██    ▓██░        ▒██░   ▒██  ▀█▄  ▒██▒ ▄██         */"
echo "/*         ▓██▄██▓       ▒██    ▒██         ▒██░   ░██▄▄▄▄██ ▒██░█▀           */"
echo "/*          ▓███▒    ██▓ ▒██▒   ░██▒ ██▓    ░██████▒▓█   ▓██▒░▓█  ▀█▓         */"
echo "/*          ▒▓▒▒░    ▒▓▒ ░ ▒░   ░  ░ ▒▓▒    ░ ▒░▓  ░▒▒   ▓▒█░░▒▓███▀▒         */"
echo "/*          ▒ ░▒░    ░▒  ░  ░      ░ ░▒     ░ ░ ▒  ░ ▒   ▒▒ ░▒░▒   ░          */"
echo "/*          ░ ░ ░    ░   ░      ░    ░        ░ ░    ░   ▒    ░    ░          */"
echo "/*          ░   ░     ░         ░     ░         ░  ░     ░  ░ ░               */"
echo "/*                    ░               ░                            ░          */"
echo "/*                                                                            */"
echo "/******************************************************************************/"
printf "                              /*Project Builder*/\n\n"
printf "Hi! I'm Aethia and I'll guide you to build your Project\n\n"


printf "********************************************************************************\n"

printf "By the way, What's the name of this one?\n\n\t"
read name
printf "\nAnd his path?\n\n\t"
read path
printf "\nCreation of $path$name..."
mkdir $path$name
printf "\033[32m[OK]\033[0m"
printf "\nCreation of $path$name/srcs..."
mkdir $path$name/srcs
printf "\033[32m[OK]\033[0m"
printf "\nCreation of $path$name/incs..."
mkdir $path$name/incs
printf "\033[32m[OK]\033[0m\n\n"

printf "********************************************************************************\n"

printf "Thanks! Now what do you want for the .gitignore?\n"
printf "\tD (Default settings)\n"
printf "\tC (Custom settings)\n"
printf "\tN (No .gitignore)\n"
printf "\tNe (Nothing Else)\n\n\t"
git=BEGIN
while [ $git != "END" ];do
	read git
	if [ $git == "D" ];then
		printf "Creation of .gitignore..."
		echo -e "*.o\n*.out\n*.a\n*.dSYM\n\n" >> $path$name/.gitignore
		printf "\033[32m[OK]\033[0m\n\n"
		git=END
	elif [ $git == "C" ];then
		ft_gitignore
		git=END
	elif [ $git == "N" ];then
		break 1
	elif [ $git == "Ne" ];then
		exit 1
	else
		printf "\nSorry I don't understand your answer\n\n\t"
	fi
done

printf "********************************************************************************\n"


printf "Do you want a Makefile ?\n"
printf "\tYes\n"
printf "\tNe (Nothing else)\n\n\t"
read makefile
if [ $makefile == "Yes" ];then
	printf "\nWill you create a library (L) or an exec (E) ?\n\n\t"
	read makefile
	make=$path$name/Makefile
	printf "\nCreation of $path$name/Makefile..."
	echo "# **************************************************************************** #" >> $make
	echo "#                                                           LE - /             #" >> $make
	echo "#                                                               /              #" >> $make
	echo "#    Makefile                                         .::    .:/ .      .::    #" >> $make
	echo "#                                                  +:+:+   +:    +:  +:+:+     #" >> $make
	echo "#    By: jmonneri <marvin@le-101.fr>                +:+   +:    +:    +:+      #" >> $make
	echo "#                                                  #+#   #+    #+    #+#       #" >> $make
	echo "#    Created: 2017/12/14 19:15:23 by jmonneri     #+#   ##    ##    #+#        #" >> $make
	echo "#    Updated: 2018/03/14 14:22:54 by jmonneri    ###    #+. /#+    ###.fr      #" >> $make
	echo "#                                                          /                   #" >> $make
	echo "#                                                         /                    #" >> $make
	echo "# **************************************************************************** #" >> $make
	printf "\n" >> $make
	echo ".PHONY: all clean fclean re" >> $make
	printf "\n" >> $make
	if [ $makefile == "L" ];then
		echo "NAME = $name" >> $make
	else
		echo "NAME = $name.a" >> $make
	fi
	echo "CC = gcc" >> $make
	echo "CC_FLAGS = -Wall -Wextra -Werror" >> $make
	echo "PATH_OBJ = ./objs/" >> $make
	echo "PATH_SRC = ./srcs/" >> $make
	echo "PATH_INC = ./incs/" >> $make
	echo 'INC = $(addprefix $(PATH_INC), libft.h )' >> $make
	printf "\033[32m[OK]\033[0m"
	printf "\nDo you want your Libft ?\n"
	printf "\tYes\n"
	printf "\tNo\n\n\t"
	read libft
	if [ $libft == "Yes" ];then
		ft_libft
		echo 'PATH_LIBFT = $(PATH_SRC)libft/' >> $make
	fi
	printf "\n" >> $make
	printf "\n#******************************************************************************#\n" >> $make
	echo "                                  $name" >> $make
	echo "#******************************************************************************#" >> $make
	printf "\n" >> $make
	echo 'PATH_SRC_PRINTF = $(PATH_SRC)printf/' >> $make
	echo 'PATH_OBJ_PRINTF = $(PATH_OBJ)printf/' >> $make
	echo 'FILES_PRINTF = ' >> $make
	echo 'OBJ_PRINTF = $(addprefix $(PATH_OBJ_PRINTF), $(addsuffix .o, $(FILES_PRINTF)))' >> $make
	echo 'SRC_PRINTF = $(addprefix $(PATH_SRC_PRINTF), $(addsuffix .c, $(FILES_PRINTF)))' >> $make
	printf "\n" >> $make
	echo '#******************************************************************************#' >> $make
	echo '#                                     ALL                                      #' >> $make
	echo '#******************************************************************************#' >> $make
	printf "\n" >> $make
	echo 'PATHS_OBJ = $(PATH_OBJ) $(PATH_OBJ_PRINTF)' >> $make
	echo 'OBJ = $(OBJ_PRINTF)' >> $make
	echo 'SRC = $(SRC_PRINTF)' >> $make
	echo 'FILES = $(FILES_PRINTF)' >> $make
	printf "\n" >> $make
	echo '#******************************************************************************#' >> $make
	echo '#                                    RULES                                     #' >> $make
	echo '#******************************************************************************#' >> $make
	printf "\n" >> $make
	echo 'all: $(NAME)' >> $make
	printf "\n" >> $make
	printf "clean:\n\t" >> $make
	printf '@printf "\\n\\033[1m🦄 🦄 🦄 🦄 SUPPRESSION DES OBJETS🦄 🦄 🦄 🦄\\033[0m\\n"' >> $make
	printf "\n\t" >> $make
	printf '@rm -f $(NAME)' >> $make
	printf "\n\n" >> $make
	printf "fclean: clean" >> $make
	printf "\n\t" >> $make
	printf '@printf "\\n\\033[1m🦄 🦄 🦄 🦄 SUPPRESSION DE $(NAME)🦄 🦄 🦄 🦄\\033[0m\\n"' >> $make
	printf "\n\t" >> $make
	printf '@rm -rf $(NAME)' >> $make
	printf "\n\n" >> $make
	printf "re: fclean all" >> $make
	printf "\n\n" >> $make
	echo '#******************************************************************************#' >> $make
	echo "                                 Comp $name" >> $make
	echo '#******************************************************************************#' >> $make
	printf "\n\n" >> $make
	printf '$(NAME): $(PATHS_OBJ) $(OBJ)' >> $make
	if [ $libft == "-Yes" ];then
		printf ' $(PATH_LIBFT)libft.a' >> $make
	fi
	printf '\n\t' >> $make
	printf '@printf \"\\n\\033[1m🦄 🦄 🦄 🦄 CREATION DE' >> $make
	printf " $name" >> $make
	echo '🦄 🦄 🦄 🦄\\033[0m\\n"' >> $make
	printf "\t" >> $make
	if [ $makefile == "-L" ];then
		printf '@ar rcs $(NAME) $(OBJ)' >> $make
		if [ $libft == "-Yes" ];then
			printf ' -L $(PATH_LIBFT) -lft' >> $make
		fi
		printf "\n\t" >> $make
		echo '@printf "  👍  👍  👍 \\033[1mLIBRAIRIE CREEE\\033[0m👍  👍  👍\\n\\n"' >> $make
	elif [ $makefile == "-E" ];then
		printf '@$(CC) $(CC_FLAGS) $(OBJ) -I $(PATH_INC) -o $(NAME)' >> $make
		if [ $libft == "-Yes" ];then
			printf ' -L $(PATH_LIBFT) -lft' >> $make
		fi
		printf "\n\t" >> $make
		echo '@printf "  👍  👍  👍 \\033[1mEXECUTABLE CREE\\033[0m👍  👍  👍\\n\\n"' >> $make
	fi
	printf "\n\n" >> $make
	printf '$(PATH_OBJ)%.o: $(PATH_SRC)%.c $(INC)' >> $make
	printf "\n\t" >> $make
	printf '@printf "0️⃣  Compilation de \\033[1m$<\033[0m en \\033[1m$@\033[0m"' >> $make
	printf '\n\t@$(CC) $(CC_FLAGS) -o $@ -c $< -I $(PATH_INC)' >> $make
	printf "\n\t" >> $make
	echo '@printf "   \\033[0;32m[OK]\\033[0m\\n"' >> $make
	printf "\n" >> $make
	if [ $libft == 'Yes' ];then
		printf '$(PATH_LIBFT)libft.a:\n\t' >> $make
		printf '@make -C $(PATH_LIBFT)\n\n' >> $make
	fi
	printf '$(PATHS_OBJ):\n\t@mkdir $@\n' >> $make
fi

printf "\n********************************************************************************\n"


echo "done"
