// https://go.developpez.com/tutoriels/go-par-l-exemple/#LI

package main

import (
	"errors"
	"fmt"
)

func main() {
	const name string = "jojo"
	i, j, k := 0, 0, 0
	fmt.Println("hello world")
	fmt.Printf("i = %d | j = %d | k = %d\n", i, j, k)
	fmt.Println("My name is " + name)
	for i < 5 {
		fmt.Printf("i = %d\n", i)
		i++
	}
	for m := 0; m < 5; m++ {
		fmt.Printf("m = %d\n", m)
	}
	for {
		if k == 5 {
			break
		}
		fmt.Printf("k = %d\n", k)
		k++
	}
	fmt.Print("write ", i, " as ")
	switch i {
	case 1:
		fmt.Println("one")
	case 2:
		fmt.Println("two")
	case 3:
		fmt.Println("three")
	case 4:
		fmt.Println("four")
	case 5:
		fmt.Println("five")
	}
}

// F   is a function of hole
func F() (int, string, error) {
	return 0, "", (errors.New("error"))
}
