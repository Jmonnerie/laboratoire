<?PHP
# **************************************************************************** #
#                                                           LE - /             #
#                                                               /              #
#    heritage.php                                     .::    .:/ .      .::    #
#                                                  +:+:+   +:    +:  +:+:+     #
#    By: jmonneri <marvin@le-101.fr>                +:+   +:    +:    +:+      #
#                                                  #+#   #+    #+    #+#       #
#    Created: 2018/05/24 12:42:16 by jmonneri     #+#   ##    ##    #+#        #
#    Updated: 2018/05/24 12:42:16 by jmonneri    ###    #+. /#+    ###.fr      #
#                                                          /                   #
#                                                         /                    #
# **************************************************************************** #

interface IExemple
{
	function foo();

}

abstract class ExempleabsA
{
	abstract public function foo();

}

class ExempleA
{
	public $att = 21;
	protected $_protectedAtt = 84;
	private $_privateATT = 42;

	public function __construct()
	{
		print ( 'Constructor ExempleA called' . PHP_EOL );
		return;
	}

	public function __destruct()
	{
		print ( 'Destructor ExempleA called' . PHP_EOL );
		return;
	}

	public function pubFoo()
	{
		print ( 'Function pubFoo from class A' . PHP_EOL );
		return;
	}

	private function _privFoo()
	{
		print ( 'Function _privFoo from class A' . PHP_EOL );
		return;
	}

	protected function _proFoo()
	{
		print ( 'Function _proFoo from class A' . PHP_EOL );
		return;
	}
}

class ExempleB extends ExempleA
{

	public function __construct()
	{
		parent::__construct();
		print ( 'Constructor ExempleB called' . PHP_EOL );
		$this->att = 42;
		return;
	}

	public function __destruct()
	{
		parent::__destruct();
		print ( 'Destructor ExempleB called' . PHP_EOL );
		return;
	}

	public function pubFoo()
	{
		print ( 'Function pubFoo from class B' . PHP_EOL );
		parent::pubFoo();
		return;
	}

}

$instance = new ExempleA();
$instance->foo();
$instanceB = new ExempleB();
$instanceB->foo();
?>
