<?PHP
# **************************************************************************** #
#                                                           LE - /             #
#                                                               /              #
#    test.php                                         .::    .:/ .      .::    #
#                                                  +:+:+   +:    +:  +:+:+     #
#    By: jmonneri <marvin@le-101.fr>                +:+   +:    +:    +:+      #
#                                                  #+#   #+    #+    #+#       #
#    Created: 2018/05/22 11:29:16 by jmonneri     #+#   ##    ##    #+#        #
#    Updated: 2018/05/22 11:29:16 by jmonneri    ###    #+. /#+    ###.fr      #
#                                                          /                   #
#                                                         /                    #
# **************************************************************************** #

Class Exemple 
{

	# convention: aucune variable en public ==> il faut les mettre en private avec getters et/ou setters#
	const CST1 = 1;
	const CST2 = 2;

	public static $nbInstances = 0; # variable presente uniquement dans la classe (ici, permet de compter le nombre d'instances) #

	public $publicFoo = 0; # ! constante ! #
	private $_privateFoo = 'hello'; # ! constante ! #

	function __construct()
	{
		print( 'Constructor called' . PHP_EOL );
		print( 'Les constantes de cette classe sont : ' . self::CST1 . ' et ' . self::CST2 );
		self::$nbInstances++; # Ici on incremente la variable statique de 1 des qu'on cree une instance #
		return;
	}

	function __destruct() # SI rien de precise, public par defaut #
	{
		print( 'Destructor called' . PHP_EOL );
		self::$nbInstances--; # Ici on desincremente la variable statique de 1 des qu'on detruit une instance #
		return;
	}

	function getPrivateFoo() # convention: get[nom de varible sans underscore et avec une maj] #
	{
		return ($this->$_privateFoo);
	}

	function setPrivateFoo( $val ) # convention: set[nom de varible sans underscore et avec une maj] #
	{
		return ($this->$_privateFoo = $val);
	}

	function __get( $var ) # est appele en cas de tentative d'acces a une variable inexistante/privee #
	{
		print( 'Tu ne peut pas acceder a: ' . $var . PHP_EOL );
		return ("quelquechose");
	}

	function __call( $f /* Nom de la fonction */, $arg/* Tableau des arguments*/) #  est appele en cas de tentative d'acces a une METHODE inexistante/privee
	{
		print( 'fonctionnne un peu comme __get/__set' );
		return;
	}

	static function __callstatic( $f, $args) #  est appele en cas de tentative d'acces a une METHODEi STATIQUE inexistante/privee
	{
		print( 'fonctionnne un peu comme __get/__set' );
		return;
	}

	function __set( $var, $value ) # est appele en cas de tentative de modification d'une variable inexistant/privee #
	{
		print( 'Tu ne peut pas modifier: ' . $var . PHP_EOL );
		return;
	}

	function __tostring() # Appele lorsque l'on appele l'instance a un endoirt ou une string est attendue (ex: print($instance); ) #
	{
		return 'Exemple( $publicFoo = ' . $this->publicFoo . ', $_privateFoo =  ' . $this->_privateFoo . ')';
	}

	function __invoke() # fonction appelee lorsque $Exemple() est appelee #
	{
		return ( $publicFoo . $_privateFooo );
	}

	function __clone() # Clone l'instance (Ex: $instance2 = clone $instance; )#
	{
		print( 'Clone called' . PHP_EOL );
		return;
	}

	function publicBar()
	{
		print( 'Method publicBar called' . PHP_EOL );
		return;
	}

	private function _privateBar()
	{
		print( 'Method _privateBar called' . PHP_EOL );
		return;
	}

	static function doc()
	{
		return 'cette fonction existe uniquement dans la classe et pas dans l\'instance (impossible d\'utiliser $this ici)';
	}
	# $this ==> fait reference a l'instance courante #
	# self ==> fait reference a la classe courante #
}

$instance = new Exemple();
# Exemple::CST1 fait reference a CST1 se trouvant dans la classe Exemple #
?>
